import com.sun.org.apache.xpath.internal.operations.Bool
import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.buildJsonArray
import kotlinx.serialization.json.encodeToJsonElement
import java.io.File
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.listDirectoryEntries


@Serializable
class Problema(
    val enunciat: String,
    val inputPublic: String,
    val outputPublic: Int,
    val inputPrivat: String,
    val outputPrivat: Int,
    var resolt: Boolean,
    var sortir: Boolean,
    var intents: Int,
    var intentsUsuari: String
)

fun menuEstudiant() {
    println("MENÚ D'ESTUDIANT")
    println("1. Seguir amb l'itinerari d'aprenentatge")
    println("2. Llista de problemes i triar-ne un")
    println("3. Consultar històric de problemes resolts")
    println("4. Ajuda")
    println("0. Sortir")
}

fun menuProfessor() {
    println("MENÚ DEL PROFESSOR")
    println("1. Afegir nous problemes")
    println("2. Treure un report de la feina de l'alumne")
    println("0. Sortir")
}

fun submenuProfessorOpcio2() {
    println("SUBMENÚ OPCIÓ 2")
    println("1. Treure una puntuació en funció dels problemes resolts")
    println("2. Descomptar per intents")
    println("3. Mostrar-ho de manera més o menys gràfica")
    println("0. Sortir")
}

fun contrasenyaProfessor() {
    var contrasenya = "contrasenya"
    var contrasenyaUsuari = ""
    do {
        println("Escriu la contrasenya:")
        contrasenyaUsuari = readln().toString()
    } while (contrasenya != contrasenyaUsuari)
}

fun contarFitxes(path: Path): Int {
    if (path.exists()) {
        val files = path.listDirectoryEntries()
        return files.size
    }
    return 0
}

fun crearFitxer(file: File, content: String = "") {
    if (!file.exists()) {
        val correcto = file.createNewFile()
        if (correcto==true) {
            file.writeText(content)
        }
    }
}

fun main() {
    println("Introdueix que ets:")
    println("1. Estudiant")
    println("2. Professor")
    var professorAlumne = readln().toInt()
    while (professorAlumne != 1 && professorAlumne != 2) {
        println("La opció seleccionada ha de ser 1 o 2")
        professorAlumne = readln().toInt()
    }

    // ESTUDIANT
    if (professorAlumne == 1) {
        do {
            menuEstudiant()
            println("Tria una opció:")
            var opcioMenuEstudiant = readln().toInt()
            while (opcioMenuEstudiant < 0 || opcioMenuEstudiant > 4) {
                println("La opció seleccionada ha d'estar entre 0 i 4")
                opcioMenuEstudiant = readln().toInt()
            }
            when (opcioMenuEstudiant) {
                1 -> {
                    println("Has triat seguir amb l'itinerari d'aprenentatge")
                    var numeroFitxers = contarFitxes(Path("./problemes/"))
                    for (i in 1..numeroFitxers) {
                        val nomFitxer = "problemes/problema" + i + ".json"
                        val file = File(nomFitxer)
                        val fileData = file.readText()
                        val json = Json.decodeFromString<Problema>(fileData)
                        if (json.resolt == false) {
                            println(json.enunciat)
                            println("Input del joc de proves públic:")
                            println(json.inputPublic)
                            println("Output del joc de proves públic:")
                            println(json.outputPublic)
                            println("Vols resoldre el problema $i? Introdueix un 1 per resoldre'l o un 0 per passar al següent")
                            var resoldreProblema = readln().toInt()
                            while (resoldreProblema != 0 && resoldreProblema != 1) {
                                println("La opció introduïda ha de ser 0 o 1")
                                resoldreProblema = readln().toInt()
                            }
                            if (resoldreProblema == 1) {
                                println("Input del joc de proves privat:")
                                println(json.inputPrivat)
                                do {
                                    println("Introdueix el resultat o un 0 per sortir i passar al següent problema")
                                    if (json.intents >= 1) {
                                        println("Els intents incorrectes que ja has probat son: " + json.intentsUsuari)
                                    }
                                    var resultatUsuari = readln().toInt()
                                    if (resultatUsuari == json.outputPrivat) {
                                        println("HAS ENCERTAT EL RESULTAT!!")
                                        json.resolt = true
                                    } else if (resultatUsuari == 0) {
                                        println("Has triat sortir")
                                        json.sortir = true
                                    } else {
                                        println("Resultat incorrecte, torna a intentar-ho")
                                        json.intentsUsuari += resultatUsuari.toString() + "  "
                                    }
                                    json.intents++
                                } while (json.resolt == false && json.sortir == false)
                                if (json.sortir==true){
                                    json.sortir=false
                                }
                                file.writeText(Json.encodeToString(json))
                            } else {
                                println("Has triat passar al següent problema")
                            }
                        } else {
                            println("El problema $i està resolt")
                        }
                    }
                }

                2 -> {
                    println("Has triat l'opció de veure la llista de problemes i triar-ne un")
                    var numeroFitxers = contarFitxes(Path("./problemes/"))
                    for (i in 1..numeroFitxers) {
                        val nomFitxer = "problemes/problema" + i + ".json"
                        val file = File(nomFitxer)
                        val fileData = file.readText()
                        val json = Json.decodeFromString<Problema>(fileData)
                        println(json.enunciat)
                    }
                    println("Tria un problema:")
                    var triarProblema = readln().toInt()
                    while (triarProblema < 1 || triarProblema > numeroFitxers) {
                        println("El problema seleccionat no existeix, ha d'estar entre 1 i $numeroFitxers")
                        triarProblema = readln().toInt()
                    }
                    val nomFitxer = "problemes/problema" + triarProblema + ".json"
                    val file = File(nomFitxer)
                    val fileData = file.readText()
                    val json = Json.decodeFromString<Problema>(fileData)
                    if (json.resolt == true) {
                        println("Aquest problema ja ha estat resolt")
                    } else {
                        println(json.enunciat)
                        println("Input del joc de proves públic:")
                        println(json.inputPublic)
                        println("Output del joc de proves públic:")
                        println(json.outputPublic)
                        println("Vols resoldre aquest problema? Introdueix un 1 per resoldre'l o un 0 per passar al següent")
                        var resoldre = 2
                        while (resoldre != 0 && resoldre != 1) {
                            resoldre = readln().toInt()
                        }
                        if (resoldre == 1) {
                            println("Input del joc de proves privat:")
                            println(json.inputPrivat)
                            do {
                                println("Introdueix el resultat o un 0 per sortir i passar al següent problema")
                                if (json.intents >= 1) {
                                    println("Els intents incorrectes que ja has probat son: " + json.intentsUsuari)
                                }
                                var resultatUsuari = readln().toInt()
                                if (resultatUsuari == json.outputPrivat) {
                                    println("HAS ENCERTAT EL RESULTAT!!")
                                    json.resolt = true
                                } else if (resultatUsuari == 0) {
                                    println("Has triat sortir")
                                    json.sortir = true
                                } else {
                                    println("Resultat incorrecte, torna a intentar-ho")
                                    json.intentsUsuari += resultatUsuari.toString() + "  "
                                }
                                json.intents++
                            } while (json.resolt == false && json.sortir == false)
                            if (json.sortir==true){
                                json.sortir=false
                            }
                            file.writeText(Json.encodeToString(json))
                        }
                    }
                }

                3 -> {
                    println("Has triat consultar històric de problemes")
                    var numeroFitxers = contarFitxes(Path("./problemes/"))
                    for (i in 1..numeroFitxers) {
                        val nomFitxer = "problemes/problema" + i + ".json"
                        val file = File(nomFitxer)
                        val fileData = file.readText()
                        val json = Json.decodeFromString<Problema>(fileData)
                        if (json.resolt == true) {
                            println("\u001b[42mPROBLEMA $i\u001b[0m")
                            println("El problema $i ha estat resolt")
                            println("Intents realitzats al problema $i: ${json.intents}")
                        } else {
                            println("\u001b[43mPROBLEMA $i\u001b[0m")
                            println("El problema $i no ha estat resolt")
                            if (json.intents!=0){
                                println("Intents realitzats al problema $i: ${json.intents}")
                            }
                        }
                        println()
                    }

                }

                4 -> {
                    println("Ajuda")
                    println("En aquest apartat veuràs quina funció té cada opció de menú")
                    println("Opció 1. Seguiràs amb l'itinerari d'aprenentatge, és a dir, et mostrarà per ordre els problemes que et queden per resoldre")
                    println("Opció 2. Et mostrarà un llistat amb els enunciats dels problemes disponibles i podràs triar quin vols resoldre")
                    println("Opció 3. Et mostra quins problemes han estat resolts i quins no, a més si s'ha intentat resoldre el problema mostra el número d'intents")
                    println("Opció 4. Mostra l'ajuda del programa")
                    println("Opció 0. Sortir")

                }
                0 -> println("Fins aviat!")
            }
        } while (opcioMenuEstudiant != 0)
    }

    // PROFESSOR
    else {
        contrasenyaProfessor()
        do {
            menuProfessor()
            println("Tria una opció:")
            var opcioMenuProfessor = readln().toInt()
            while (opcioMenuProfessor < 0 || opcioMenuProfessor > 2) {
                println("La opció seleccionada ha d'estar entre 0 i 2")
                opcioMenuProfessor = readln().toInt()
            }
            when (opcioMenuProfessor) {
                1 -> {
                    var numeroFitxers = contarFitxes(Path("./problemes/"))
                    var numFinalFitxer = numeroFitxers + 1
                    var nomFitxer = "problemes/problema" + numFinalFitxer + ".json"
                    var textAfegir = "{\"enunciat\":\""
                    println("Has triat afegir un nou problema, introdueix els següents atributs:")
                    print("Enunciat: ")
                    var enunciat = readln().toString()
                    print("Input públic: ")
                    var inputPublic = readln().toString()
                    print("Output públic: ")
                    var outputPublic = readln().toInt()
                    print("Input privat: ")
                    var inputPrivat = readln().toString()
                    print("Output privat: ")
                    var outputPrivat = readln().toInt()
                    textAfegir+=enunciat + "\"" + ",\"inputPublic\":" + "\"" + inputPublic + "\"" + ",\"outputPublic\":" + outputPublic + ",\"inputPrivat\":" + "\"" + inputPrivat + "\"" + ",\"outputPrivat\":" + outputPrivat + "," + "\"resolt\": false," + "\"sortir\": false," + "\"intents\": 0," + "\"intentsUsuari\": \"\"}"
                    crearFitxer(File(nomFitxer), textAfegir)
                }

                2 -> {
                    do {
                        submenuProfessorOpcio2()
                        println("Tria una opció:")
                        var opcioSubmenu = readln().toInt()
                        while (opcioSubmenu < 0 || opcioSubmenu > 3) {
                            println("La opció seleccionada ha d'estar entre 0 i 3")
                            opcioSubmenu = readln().toInt()
                        }
                        when (opcioSubmenu){
                            1-> {
                                println("Has triat treure una puntuació en funció dels problemes resolts")
                                var problemesResolts = 0
                                var numeroFitxers = contarFitxes(Path("./problemes/"))
                                for (i in 1..numeroFitxers){
                                    val nomFitxer = "problemes/problema" + i + ".json"
                                    val file = File(nomFitxer)
                                    val fileData = file.readText()
                                    val json = Json.decodeFromString<Problema>(fileData)
                                    if (json.resolt==true) {
                                        problemesResolts++
                                    }
                                }
                                var percentatgeResolts = (problemesResolts.toDouble()/numeroFitxers.toDouble())*100
                                println("S'han resolt $problemesResolts de $numeroFitxers problemes")
                                println("El percentatge de problemes resolts és del ${String.format("%.2f", percentatgeResolts)}% del total")
                            }
                            2->{
                                var problemesResolts = 0
                                var intentsTotalsAlsProblemes = 0
                                var sumaTotalPuntuacioProblemes = 0
                                var numeroFitxers = contarFitxes(Path("./problemes/"))
                                for (i in 1..numeroFitxers){
                                    val nomFitxer = "problemes/problema" + i + ".json"
                                    val file = File(nomFitxer)
                                    val fileData = file.readText()
                                    val json = Json.decodeFromString<Problema>(fileData)
                                    var notaProblema = 0
                                    if (json.intents!=0){
                                        problemesResolts++
                                        intentsTotalsAlsProblemes+=json.intents
                                        notaProblema = when (json.intents){
                                            1-> 10
                                            2-> 9
                                            3-> 8
                                            4-> 7
                                            5-> 6
                                            6-> 5
                                            7-> 4
                                            8-> 3
                                            9-> 2
                                            10-> 1
                                            else-> 0
                                        }
                                        sumaTotalPuntuacioProblemes+=notaProblema
                                    }

                                }
                                println("Problemes resolts: $problemesResolts de $numeroFitxers")
                                var notaProblemesResolts = sumaTotalPuntuacioProblemes.toDouble()/problemesResolts.toDouble()
                                println("NOTA DELS PROBLEMES RESOLTS: ${String.format("%.2f", notaProblemesResolts)}%")
                            }
                            3-> {
                                var numeroFitxers = contarFitxes(Path("./problemes/"))
                                for (i in 1..numeroFitxers){
                                    val nomFitxer = "problemes/problema" + i + ".json"
                                    val file = File(nomFitxer)
                                    val fileData = file.readText()
                                    val json = Json.decodeFromString<Problema>(fileData)
                                    if (json.resolt){
                                        println("\u001b[42mEl problema $i esta resolt\u001b[0m")
                                    }
                                    else {
                                        println("\u001b[43mEl problema $i no està resolt\u001b[0m")
                                    }
                                }
                            }
                        }
                    } while (opcioSubmenu != 0)
                }
                0 -> println("Fins aviat!")
            }
        } while (opcioMenuProfessor != 0)
    }
}